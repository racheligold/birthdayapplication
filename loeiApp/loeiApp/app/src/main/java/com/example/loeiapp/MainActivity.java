package com.example.loeiapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;


import static com.example.loeiapp.FriendInfo.setInfo;
import static com.example.loeiapp.MyBroadcastReceiver.setActivity;
import static com.example.loeiapp.SettingsFragment.SERVER_KEY;

public class MainActivity extends AppCompatActivity {
    public static final String MYTAG = "MYTAG";
    AppDatabase instance;
    public static Activity myactivity;
    static boolean spScreenOn = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        myactivity = this;
        boolean serverStatus =  PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SERVER_KEY, false);
        if(serverStatus){
            startService();
        }
        setContentView(R.layout.activity_main);
        instance = AppDatabase.getAppDatabase(getApplicationContext().getApplicationContext());
        setActivity(this);
        registerNetwork();
    }

    private void registerNetwork() {
        IntentFilter intentFilter = new IntentFilter(Intent.ACTION_AIRPLANE_MODE_CHANGED);
        intentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(new MyBroadcastReceiver(), intentFilter);

    }

    public void startService() {
        Intent serviceIntent = new Intent(this, MyService.class);
        serviceIntent.setAction("start");
        startService(serviceIntent);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.settings:
                if(!spScreenOn){
                    spScreenOn = true;
                    FragmentManager fm = getSupportFragmentManager();
                    Fragment toHide = fm.findFragmentById(R.id.fragment);
                    FragmentTransaction ft = fm.beginTransaction();
                    if (toHide != null) {
                        ft.hide(toHide);    // hide main fragment.
                    }
                    ft.add(R.id.settings_container, new SettingsFragment())
                            .addToBackStack("bbb")
                            .commit();
                }

                return true;

            case R.id.Adding :
                Intent intent = new Intent(this.getApplicationContext(), FriendInfo.class);
                startActivityForResult(intent, 1);
                setInfo(instance);
                return true;

            case R.id.about :
                Intent aboutIntent = new Intent(this.getApplicationContext(), AboutActivity.class);
                startActivity(aboutIntent);

                //not necessary anymore

                /*final AlertDialog.Builder AB_builder = new AlertDialog.Builder(this);
                AB_builder.setTitle("About");
                AB_builder.setMessage(R.string.about_msg);

                AlertDialog.Builder nik = AB_builder.setPositiveButton("Now I Know", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                AlertDialog ABdialog = AB_builder.create();
                ABdialog.show();*/
                return true;

            case R.id.birthdayGallery:
                Intent galleryIntent = new Intent(this.getApplicationContext(), GalleryActivity.class);
                startActivity(galleryIntent);
                return true;

            case R.id.exit:
                final AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle(R.string.exit_dialog_title);
                builder.setMessage(R.string.exit_dialog_text);
                AlertDialog.Builder yes = builder.setPositiveButton(R.string.dialog_yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });
                // Set the alert dialog no button click listener
                builder.setNegativeButton(R.string.dialog_no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                AlertDialog dialog = builder.create();
                // Display the alert dialog on interface
                dialog.show();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
