package com.example.loeiapp;

import java.util.List;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

@Dao
public interface FriendDAO {

    @Query("SELECT * FROM FriendEntity")
    List<FriendEntity> getAll();


    @Query("DELETE FROM FriendEntity")
    void deleteAll();

    @Query("SELECT COUNT(uid) FROM FriendEntity")
    int rowcount();


    @Query("SELECT * FROM FriendEntity FE WHERE FE.uid = :userId")
    FriendEntity loadFriendById(int userId);

    @Insert
    void insert(FriendEntity user);

    @Delete
    void delete(FriendEntity user);

    //-----------------MessageList------------------------

    @Query("SELECT * FROM MessageListEntity ML WHERE ML.wasSent = 'No'")
    List<MessageListEntity> getAllMessages();


    @Query("UPDATE MessageListEntity SET wasSent = 'Yes' WHERE MessageListEntity.mid = :mId")
    void setMessageSent(int mId);

    @Insert
    void insert(MessageListEntity message);

    @Delete
    void delete(MessageListEntity message);

    @Query("DELETE FROM MessageListEntity WHERE wasSent='Yes'")
    void deleteAllSentMessages();

    @Query("DELETE FROM MessageListEntity")
    void deleteAllMessages();

    @Query("SELECT COUNT(ML.uid) FROM MessageListEntity ML WHERE ML.uid = :userId")
    int isMessageExistFor(int userId);

    @Query("SELECT COUNT(mid) FROM MessageListEntity")
    int rowMessageCount();

    @Query("SELECT COUNT(ML.uid) FROM MessageListEntity ML WHERE wasSent='Yes'")
    int countAllSentMessages();

}
