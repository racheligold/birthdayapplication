package com.example.loeiapp;

import android.app.Activity;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.List;

public class GalleryAdapter extends ArrayAdapter<Picture> {
    Context context;
    private List<Picture> objects;
    public GalleryAdapter(@NonNull Context context, int resource, int textViewResourceId, @NonNull List<Picture> objects) {
        super(context, resource, textViewResourceId, objects);
        this.context = context;
        this.objects = objects;
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater layoutInflater = ((Activity)context).getLayoutInflater();
        View view = layoutInflater.inflate(R.layout.picture, parent, false);
        TextView author = (TextView)view.findViewById(R.id.tvAuthor);
        TextView description = (TextView)view.findViewById(R.id.tvDescription);
        ImageView image = (ImageView) view.findViewById(R.id.ivPic);
        Picture temp = objects.get(position);
        author.setText(temp.getAuthor());//set picture's author
        description.setText(temp.getDescription()); //set picture's description
        image.setImageBitmap(BitmapFactory.decodeByteArray(temp.getImage(), 0,temp.getImage().length));
        return view;
    }

    //getter for objects property
    public List<Picture> getObjects() {
        return this.objects;
    }

    //setter for objects property
    public void setObjects(List<Picture> objects) {
        this.objects = objects;
    }
}
