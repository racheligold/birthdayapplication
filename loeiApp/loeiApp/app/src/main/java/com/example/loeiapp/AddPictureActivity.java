package com.example.loeiapp;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.TimePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Calendar;

public class AddPictureActivity extends AppCompatActivity implements View.OnClickListener {
    EditText userName, picDescription;
    Button takePicture, uploadPicture;
    Helper helper;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_picture);
        connectComponents();
        takePicture.setOnClickListener(this);
        uploadPicture.setOnClickListener(this);
    }

    private void connectComponents() {
        userName = (EditText)findViewById(R.id.etPictureAuthor);
        picDescription = (EditText)findViewById(R.id.etPicDescription);
        takePicture = (Button)findViewById(R.id.btnTakePicture);
        uploadPicture = (Button) findViewById(R.id.btnUploadPicture);
        helper = new Helper(this);
    }

    @Override
    public void onClick(View v) {
        //check if the user enter his user name and picture's description
        if(userName.getText().toString().length() == 0 || picDescription.getText().toString().length() == 0)
        {
            Toast.makeText(this, "Please enter all details", Toast.LENGTH_LONG).show();
            return;
        }
        if(v == takePicture)
        {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(intent, 0);
        }
        else if(v == uploadPicture)
        {
            Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
            photoPickerIntent.setType("image/*");
            startActivityForResult(photoPickerIntent, 1);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 0) //if it comes from the camera
        {
            if(resultCode == RESULT_OK)
            {
                Bitmap bitmap = (Bitmap)data.getExtras().get("data");
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                helper.addPicture(new Picture(userName.getText().toString(),
                        picDescription.getText().toString(),
                        stream.toByteArray())); //add picture to db
                bitmap.recycle();
                Toast.makeText(this, "The picture was taken successfully", Toast.LENGTH_LONG).show();
                finish();
            }
            else if(resultCode == RESULT_CANCELED)
            {
                Toast.makeText(this, "Please take picture", Toast.LENGTH_LONG).show();
            }
        }
        else if(requestCode == 1) //if it comes from the gallery
        {
            if (resultCode == RESULT_OK) {
                try {
                     final Uri imageUri = data.getData();
                    final InputStream imageStream = getContentResolver().openInputStream(imageUri);
                    final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    selectedImage.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                    helper.addPicture(new Picture(userName.getText().toString(),
                            picDescription.getText().toString(),
                            stream.toByteArray())); //add picture to db
                    selectedImage.recycle();
                    Toast.makeText(this, "The picture was uploaded successfully", Toast.LENGTH_LONG).show();
                    finish();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                    Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG).show();
                }

            }else {
                Toast.makeText(this, "You haven't picked image",Toast.LENGTH_LONG).show();
            }
        }
    }
}