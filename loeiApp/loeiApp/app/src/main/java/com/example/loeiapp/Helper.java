package com.example.loeiapp;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

public class Helper extends SQLiteOpenHelper {
    //constants
    private static final String DATABASE_NAME = "BirthdayGallery.db";

    private static final int DATABASE_VERSION = 4;

    private static final String TABLE_NAME = "gallery";

    private static final String COLUMN_ID = "id";

    private static final String COLUMN_AUTHOR = "author";

    private static final String COLUMN_DESCRIPTION = "description";

    private static final String COLUMN_IMAGE = "image";

    private static final String CREATE_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " ( "
            + COLUMN_ID +" INTEGER PRIMARY KEY AUTOINCREMENT, "
            + COLUMN_AUTHOR + " TEXT , "
            + COLUMN_DESCRIPTION + " TEXT, "
            + COLUMN_IMAGE + " BLOB );";

    private static final String GET_PICTURES = "SELECT * FROM " + TABLE_NAME + " ORDER BY " + COLUMN_ID + " DESC;";

    public Helper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        //create the tables
        sqLiteDatabase.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        //drop table
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS "+  TABLE_NAME);
        //then, create it
        onCreate(sqLiteDatabase);
    }

    /*function inserts picture to the gallery table
     * input: picture
     * output: none
     */
    public void addPicture(Picture picture)
    {
        SQLiteDatabase sqLiteDatabase = getWritableDatabase();
        ContentValues values = new ContentValues(); //hash map
        values.put(COLUMN_AUTHOR, picture.getAuthor());
        values.put(COLUMN_DESCRIPTION, picture.getDescription());
        values.put(COLUMN_IMAGE, picture.getImage());
        sqLiteDatabase.insert(TABLE_NAME,null,values); //nullcolumnhack default value
    }

    /*function gets all the pictures from the db
     * input: none
     * output: pictures(in ArrayList)
     */
    public ArrayList<Picture> getPictures()
    {
        ArrayList<Picture> pictures = new ArrayList<Picture>();
        String author, description;
        byte[] image;
        Cursor cursor = getWritableDatabase().rawQuery(GET_PICTURES, null);
        if(cursor.getCount() > 0)
        {
            while (cursor.moveToNext())
            {
                author = cursor.getString(cursor.getColumnIndex(COLUMN_AUTHOR));
                description = cursor.getString(cursor.getColumnIndex(COLUMN_DESCRIPTION));
                image = cursor.getBlob(cursor.getColumnIndex(COLUMN_IMAGE));
                pictures.add(new Picture(author, description, image)); //add new picture to the arrayList
            }
        }
        return pictures;
    }
}
