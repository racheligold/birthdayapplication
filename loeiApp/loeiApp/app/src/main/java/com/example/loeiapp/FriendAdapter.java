package com.example.loeiapp;
import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.ArrayList;


import androidx.recyclerview.widget.RecyclerView;
import static com.example.loeiapp.MainActivity.MYTAG;

// Create the basic adapter extending from RecyclerView.Adapter
// Note that we specify the custom ViewHolder which gives us access to our views
public class FriendAdapter extends RecyclerView.Adapter<FriendAdapter.ViewHolder> {
    private Context mContext;
    private ArrayList<FriendEntity> mFriends;
    private ClickListener clickListener;
    private MyLongClickListener myLongClickListener;

    // Provide a direct reference to each of the views within a data item
    // Used to cache the views within the item layout for fast access
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener{
        // Your holder should contain a member variable
        // for any view that will be set as you render a row
        public View itemView;
        public TextView FriendFullName;
        public TextView FriendBirthday;
        public TextView FriendAge;
        public TextView FriendAgeinDays;
        public TextView AutoSender;
        public  ImageView pAgeImage;

        // We also create a constructor that accepts the entire item row
        // and does the view lookups to find each subview
        public ViewHolder(View itemView) {
            // Stores the itemView in a public final member variable that can be used
            // to access the context from any ViewHolder instance.
            super(itemView);
            this.itemView = itemView;
            FriendFullName = (TextView) itemView.findViewById(R.id.fullname);
            FriendBirthday = (TextView) itemView.findViewById(R.id.birthday);
            FriendAge = (TextView) itemView.findViewById(R.id.age);
            FriendAgeinDays = (TextView) itemView.findViewById(R.id.indays);
            AutoSender = (TextView) itemView.findViewById(R.id.autosender);
            pAgeImage = (ImageView)itemView.findViewById(R.id.pAgeImage);
            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
        }
        @Override
        public void onClick(View v) {
            clickListener.onItemClick(getPosition());
        }
        @Override
        public boolean onLongClick(View v) {
            myLongClickListener.onItemLongClick(getPosition());
            return true;
        }
    }
    // Store a member variable for the contacts


    // Pass in the contact array into the constructor
    public FriendAdapter(Activity context, ArrayList<FriendEntity> friends) {
        mContext = context;
        mFriends = friends;
    }

    // Usually involves inflating a layout from XML and returning the holder
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // Inflate and create view only if doesn't exist.
        Context context = parent.getContext();
        mContext = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View contactView = inflater.inflate(R.layout.row_item, parent, false);
        // configure view holder
        ViewHolder viewHolder = new ViewHolder(contactView);
        return viewHolder;
    }

    // Involves populating data into the item through holder
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        /* Get Friend */
        FriendEntity friend = mFriends.get(position);

        /* Friend Name */
        viewHolder.FriendFullName.setText("Name: "+friend.getFullName());

        /* Friend Birthday */
        viewHolder.FriendBirthday.setText("Birthday: "+friend.getBirthday());

        int Age = Integer.parseInt(friend.getAge());
        int DaysToBirthday = friend.getDaysForBirthday();

        if(DaysToBirthday == 0){
            /* Friend Age */
            viewHolder.FriendAge.setText((Age)+"");
            viewHolder.FriendAgeinDays.setText("Today!");
        }else{
            /* Friend Age */
            viewHolder.FriendAge.setText((Age+1) + " in");
            viewHolder.FriendAgeinDays.setText((DaysToBirthday+364)%365+1 + " days");
        }


        if(Age <=5)viewHolder.pAgeImage.setImageResource(R.drawable.p05);
        else if(Age >5 && Age <=12)viewHolder.pAgeImage.setImageResource(R.drawable.p612);
        else if(Age >12 && Age <=17)viewHolder.pAgeImage.setImageResource(R.drawable.p1317);
        else if(Age >17 && Age <=27)viewHolder.pAgeImage.setImageResource(R.drawable.p1827);
        else if(Age >27 && Age <=65)viewHolder.pAgeImage.setImageResource(R.drawable.p2865);
        else if(Age >65)viewHolder.pAgeImage.setImageResource(R.drawable.p66plus);



        /* Friend AutoSender */
        viewHolder.AutoSender.setText("Auto-Send: "+friend.getAutosender());
    }

    // Returns the total count of items in the list
    @Override
    public int getItemCount() {
        return mFriends.size();
    }

    public void setOnItemClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public void setOnItemLongClickListener(MyLongClickListener clickListener) {
        this.myLongClickListener = clickListener;
    }
    public interface ClickListener {
        void onItemClick(int position);
    }
    public interface MyLongClickListener {
        boolean onItemLongClick(int position);
    }

    public void remove(int position) {
        try {
            mFriends.remove(position);
            notifyDataSetChanged();
        }
        catch (Exception e) {
            Log.e(MYTAG, e.getMessage());
        }
    }

}