package com.example.loeiapp;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;
import java.util.List;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import static com.example.loeiapp.FriendInfo.setInfo;


public class FriendListFragment extends Fragment {
    private FriendAdapter mAdapter;
    private RecyclerView rvFriends;
    AppDatabase instance;
    static View globalview;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.list_fragment, container, false);
    }


    public void ValueInjection(FriendDAO myDAO,int count){
        myDAO.deleteAll();
        for(int i=1 ; i<=count; i++){
            myDAO.insert( new FriendEntity(i-1 ,"anonimus-"+i ,"10/06/1992" ,"sms" ,"5555215554","Yes","happy birthday"));
        }
        for(int i=count+1 ; i<=count*2; i++){
            myDAO.insert( new FriendEntity(i ,"person-"+i, "23/02/1992","mail" ,"Orielgilo@gmail.com","Yes","happy birthday"));
        }
    }

    public static void ChangeBackgroundColor(View view, String style){
        switch (style){
            case "Dark":
                view.setBackgroundColor(Color.GRAY);
                break;
            case "Original":
                view.setBackgroundColor(Color.LTGRAY);
                break;
            case "Light":
                view.setBackgroundColor(Color.WHITE);
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        LoadListFromDB();
    }

    public void LoadListFromDB(){
        final FriendDAO myDAO = instance.friendDao();
        List<FriendEntity> friendsEntityDB = myDAO.getAll();
        //------------------friends list--------------------
        final ArrayList<FriendEntity> friends = new ArrayList<FriendEntity>();
        friends.addAll(friendsEntityDB);
        mAdapter = new FriendAdapter(getActivity(), friends);
        //----------------------------------------------------
        rvFriends = (RecyclerView) globalview.findViewById(R.id.rvFriends);
        rvFriends.setAdapter(mAdapter);
        rvFriends.setLayoutManager(new LinearLayoutManager(getActivity()));
        mAdapter.setOnItemLongClickListener(new FriendAdapter.MyLongClickListener() {
            @Override
            public boolean onItemLongClick(int position) {
                final int final_position = position;

                final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle("Deleting "+friends.get(position).getFullName());
                builder.setMessage("Are you sure?");
                AlertDialog.Builder yes = builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        FriendEntity fe = friends.get(final_position);
                        myDAO.delete(fe);
                        mAdapter.remove(final_position);
                    }
                });
                // Set the alert dialog no button click listener
                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
                AlertDialog dialog = builder.create();
                // Display the alert dialog on interface
                dialog.show();
                return true;
            }
        });

        mAdapter.setOnItemClickListener(new FriendAdapter.ClickListener() {
            @Override
            public void onItemClick(int position) {
                Intent intent = new Intent(getActivity(), FriendInfo.class);
                startActivityForResult(intent, 1);
                FriendEntity fe = friends.get(position);
                Log.i("111", fe.toString());
                setInfo(fe,instance);
            }
        });
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        globalview = view;
        super.onViewCreated(view, savedInstanceState);
        SharedPreferences sharedPref = getActivity().getPreferences(Context.MODE_PRIVATE);
        String useSpMethod = sharedPref.getString("Color_background", "");
        ChangeBackgroundColor(globalview, useSpMethod);
        //------------Data Base Instance---------------//
        instance = AppDatabase.getAppDatabase(getActivity().getApplicationContext());
        final FriendDAO myDAO = instance.friendDao();
        //--------------------------------------------//
        //ValueInjection(myDAO,10);
        //----------------------------------------------------------
        LoadListFromDB();

    }

}
