package com.example.loeiapp;

public class Picture {
    private String author;
    private String description;
    private byte[] image;

    //Constructor
    public Picture(String author, String description, byte[] image)
    {
        this.author = author;
        this.description = description;
        this.image = image;
    }

    //getter to author property
    public String getAuthor() {
        return this.author;
    }

    //getter to description property
    public String getDescription() {
        return this.description;
    }

    //getter to image property
    public byte[] getImage() {
        return this.image;
    }

    //setter to author property
    public void setAuthor(String author) {
        this.author = author;
    }

    //setter to description property
    public void setDescription(String description) {
        this.description = description;
    }

    //setter to image property
    public void setImage(byte[] image) {
        this.image = image;
    }
}
