package com.example.loeiapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;
import androidx.preference.SwitchPreferenceCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


public class SettingsFragment extends PreferenceFragmentCompat {
    ViewGroup global_container;
    SharedPreferences sharedPref;
    SharedPreferences.Editor editor;
    public static final String SERVER_KEY = "server";

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        setPreferencesFromResource(R.xml.preferences, rootKey); // Load the Preferences from the XML file
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        MainActivity.spScreenOn = false;
    }

    public void click_on_color(SharedPreferences.Editor editor , String color){
        editor.putString("Color_background", color);
        editor.apply();
    }

    public void ChangeBackgroundColor(ViewGroup view, String style){
        switch (style){
            case "Dark":
                view.setBackgroundColor(Color.GRAY);
                break;
            case "Original":
                view.setBackgroundColor(Color.LTGRAY);
                break;
            case "Light":
                view.setBackgroundColor(Color.WHITE);
                break;
        }
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        final SwitchPreferenceCompat switchPref;
        switchPref = (SwitchPreferenceCompat) getPreferenceManager().findPreference(SERVER_KEY);
        switchPref.setSummaryOff(R.string.server_off);
        switchPref.setSummaryOn(R.string.server_on);


        switchPref.setOnPreferenceClickListener(new SwitchPreferenceCompat.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                if(switchPref.isChecked()){
                    Intent serviceIntent = new Intent(getContext(), MyService.class);
                    serviceIntent.setAction("start");
                    getContext().startService(serviceIntent);
                }
                else{
                    Intent serviceIntent = new Intent(getContext(), MyService.class);
                    serviceIntent.setAction("stop");
                    getContext().startService(serviceIntent);
                }
                return true;
            }
        });

        global_container = container;
        sharedPref = getActivity().getPreferences(Context.MODE_PRIVATE);
        editor = sharedPref.edit();
        String useSpMethod = sharedPref.getString("Color_background", "");
        ChangeBackgroundColor(global_container, useSpMethod);


        Preference DPref = (Preference) findPreference("Darkpref");
        DPref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                global_container.setBackgroundColor(Color.GRAY);
                click_on_color(editor,"Dark");
                FriendListFragment.ChangeBackgroundColor(FriendListFragment.globalview, "Dark");
                return true;
            }
        });

        Preference OPref = (Preference) findPreference("Originalpref");
        OPref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                global_container.setBackgroundColor(Color.LTGRAY);
                click_on_color(editor,"Original");
                FriendListFragment.ChangeBackgroundColor(FriendListFragment.globalview, "Original");
                return true;
            }
        });

        Preference LPref = (Preference) findPreference("Lightpref");
        LPref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                global_container.setBackgroundColor(Color.WHITE);
                click_on_color(editor,"Light");
                FriendListFragment.ChangeBackgroundColor(FriendListFragment.globalview, "Light");
                return true;
            }
        });

        return super.onCreateView(inflater, container, savedInstanceState);
    }


}