package com.example.loeiapp;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

public class FriendInfo extends AppCompatActivity implements View.OnClickListener{
    private static final int MY_PERMISSIONS_REQUEST_SEND_SMS =22 ;
    private static EditText FriendFullName;
    private static EditText FriendBirthday;
    private static EditText FriendAge;
    private static EditText FriendInformation;
    private static CheckBox AutoSender;
    private static EditText Message;
    public static Context context;
    private Button savebtn;
    private Button editbtn;
    private Button cancelbtn;
    private ImageView sms_img , mail_img, profile_img;
    private static FriendEntity OldFE;


    private static String strFullname, strBirthday, strAge,strKindOfInfo ,strinformation ,strAutosender, strMessage;
    private  static int struid;
    static AppDatabase strinstance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.friendinfo);
        this.FriendFullName= (EditText) findViewById(R.id.FullnameInfo);
        this.FriendBirthday= (EditText) findViewById(R.id.BirthdayInfo);
        this.FriendAge= (EditText) findViewById(R.id.AgeInfo);
        this.FriendInformation= (EditText) findViewById(R.id.information);
        this.AutoSender= (CheckBox) findViewById(R.id.AutoSenderInfo);
        this.Message = (EditText) findViewById(R.id.MessageInfo);
        this.context = this;
        this.savebtn = (Button) findViewById(R.id.savebtn);
        this.editbtn = (Button) findViewById(R.id.editbtn);
        this.cancelbtn = (Button) findViewById(R.id.cancelbtn);
        this.sms_img = (ImageView) findViewById(R.id.smsimg);
        this.mail_img = (ImageView) findViewById(R.id.mailimg);
        this.profile_img = (ImageView) findViewById(R.id.FriendImage);
        savebtn.setOnClickListener(this);
        editbtn.setOnClickListener(this);
        cancelbtn.setOnClickListener(this);
        sms_img.setOnClickListener(this);
        mail_img.setOnClickListener(this);
        AutoSender.setOnClickListener(this);
        LoadBaseInfo();
        if(FriendFullName.getText().toString().equals("change me"))EditMode();
        else UnEditMode();
    }


    public void LoadBaseInfo(){
        Log.i("111", "print"+print());
        FriendFullName.setText(strFullname);
        FriendBirthday.setText(strBirthday);
        FriendAge.setText(strAge);
        if(strKindOfInfo.equals("sms"))sms_img.setBackgroundColor(Color.YELLOW);
        else if(strKindOfInfo.equals("mail")) mail_img.setBackgroundColor(Color.YELLOW);
        FriendInformation.setText(strinformation);
        if(strAutosender.equals("Yes")) AutoSender.setChecked(true);
        else AutoSender.setChecked(false);
        Message.setText(strMessage);
        Message.setMovementMethod(new ScrollingMovementMethod());

        int Age = Integer.parseInt(strAge);
        if(Age <=5)profile_img.setImageResource(R.drawable.p05);
        else if(Age >5 && Age <=12)profile_img.setImageResource(R.drawable.p612);
        else if(Age >12 && Age <=17)profile_img.setImageResource(R.drawable.p1317);
        else if(Age >17 && Age <=27)profile_img.setImageResource(R.drawable.p1827);
        else if(Age >27 && Age <=65)profile_img.setImageResource(R.drawable.p2865);
        else if(Age >65)profile_img.setImageResource(R.drawable.p66plus);
    }


    public static void setInfo(AppDatabase instance){
        final FriendDAO myDAO = instance.friendDao();
        FriendEntity fe = new FriendEntity(myDAO.rowcount()+1,"change me","1/1/1990","mail","example@gmail.com","No","");
        setInfo(fe, instance);
    }


    public static void setInfo(FriendEntity fe, AppDatabase instance){
        OldFE = fe;
        struid = fe.getUid();
        strFullname = fe.getFullName();
        strBirthday = fe.getBirthday();
        strAge = fe.getAge();
        strKindOfInfo = fe.getKindOfInfo();
        strinformation = fe.getInformation();
        strAutosender = fe.getAutosender();
        strMessage = fe.getMessage();
        strinstance = instance;
    }

    public  void Sms_func(){
        sms_img.setBackgroundColor(Color.YELLOW);
        mail_img.setBackgroundColor(Color.TRANSPARENT);
        strKindOfInfo = "sms";

        int readPhonePermissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE);
        int sendSMSPermissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS);

        if (readPhonePermissionCheck != PackageManager.PERMISSION_GRANTED || sendSMSPermissionCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE,Manifest.permission.SEND_SMS}, MY_PERMISSIONS_REQUEST_SEND_SMS);
        }

    }


    public  void mail_func(){
        sms_img.setBackgroundColor(Color.TRANSPARENT);
        mail_img.setBackgroundColor(Color.YELLOW);
        strKindOfInfo = "mail";
    }

    public void saveData(){
        String autoSenderTemp;
        String errorMessage = "";
        boolean allowSave = true;
        if(AutoSender.isChecked()){
            autoSenderTemp = "Yes";
            if(strKindOfInfo.equals("mail")){
                if(!(allowSave = isEmailValid(FriendInformation.getText().toString())))
                errorMessage = "The email you entered is invalid. ";
            }else{
                if(!(allowSave = isPhoneValid(FriendInformation.getText().toString())))
                errorMessage = "The phone number you entered is invalid. ";
            }
            if(Message.getText().toString().isEmpty()){
                errorMessage = errorMessage + "The Message field can't be empty! ";
                allowSave = false;
            }

        }
        else autoSenderTemp = "No";
        if(allowSave){
            if(FriendFullName.getText().toString().equals("change me"))FriendFullName.setText("anonymous");

            FriendEntity fe = new FriendEntity(struid, FriendFullName.getText().toString(),
                    FriendBirthday.getText().toString(),strKindOfInfo,FriendInformation.getText().toString()
                    ,autoSenderTemp,Message.getText().toString());
            final FriendDAO myDAO = strinstance.friendDao();

            myDAO.delete(OldFE);
            FriendInfo.setInfo(fe, strinstance);

            UnEditMode();
            LoadBaseInfo();
            myDAO.insert(fe);
        }
        else{
            Toast.makeText(context, errorMessage+"Please try again.",Toast.LENGTH_LONG).show();
        }
    }


    public boolean isEmailValid(String email) {
        String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\."+
                "[a-zA-Z0-9_+&*-]+)*@" +
                "(?:[a-zA-Z0-9-]+\\.)+[a-z" +
                "A-Z]{2,7}$";

        Pattern pat = Pattern.compile(emailRegex);
        if (email == null)
            return false;
        return pat.matcher(email).matches();
    }

    public boolean isPhoneValid(String s) {
        // Begins with 05 // Then contains 8 digits
        Pattern p = Pattern.compile("(05)?[0-9]{8}");
        Matcher m = p.matcher(s);
        return (m.find() && m.group().equals(s))||(s.equals("5555215554"));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.savebtn:
                saveData();
                break;

            case R.id.editbtn:
            case R.id.AutoSenderInfo:
                EditMode();
                break;


            case R.id.cancelbtn:
                UnEditMode();
                LoadBaseInfo();
                break;

            case R.id.smsimg:
                Sms_func();
                break;


            case R.id.mailimg:
                mail_func();
                break;
        }

    }

    public void EditMode(){
        FriendFullName.setEnabled(true);
        FriendBirthday.setEnabled(true);
        AutoSender.setEnabled(true);
        Message.setEnabled(true);
        editbtn.setEnabled(false);
        savebtn.setVisibility(View.VISIBLE);
        cancelbtn.setVisibility(View.VISIBLE);

        if(AutoSender.isChecked()){
            FriendInformation.setEnabled(true);
            sms_img.setEnabled(true);
            mail_img.setEnabled(true);
            sms_img.setAlpha((float) 1);
            mail_img.setAlpha((float) 1);
        }

        else{
            FriendInformation.setEnabled(false);
            sms_img.setEnabled(false);
            mail_img.setEnabled(false);
            sms_img.setAlpha((float) 0.5);
            mail_img.setAlpha((float) 0.5);
        }

    }

    public void UnEditMode(){
        FriendFullName.setEnabled(false);
        FriendBirthday.setEnabled(false);
        AutoSender.setEnabled(false);
        Message.setEnabled(false);
        editbtn.setEnabled(true);
        FriendInformation.setEnabled(false);
        sms_img.setEnabled(false);
        sms_img.setAlpha((float) 0.5);
        mail_img.setEnabled(false);
        mail_img.setAlpha((float) 0.5);
        savebtn.setVisibility(View.INVISIBLE);
        cancelbtn.setVisibility(View.INVISIBLE);
    }


    public static String print() {
        return "\nname: "+strFullname+"\nbirthday: "+strBirthday+ "\nKindOfInfo:"+strKindOfInfo +
                "\ninformation:"+ strinformation + "\nage: "+strAge+"\nautosender: "+strAutosender+""+"\nmessage: "+strMessage;

    }
}
