package com.example.loeiapp;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import android.util.Log;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Date;

import static com.example.loeiapp.MainActivity.MYTAG;
import static java.lang.String.valueOf;

@Entity
public class FriendEntity {

    @PrimaryKey
    public int uid;

    @ColumnInfo
    public String fullName;

    @ColumnInfo
    public String birthday;

    @ColumnInfo
    public String kindOfInfo; //mail/sms

    @ColumnInfo
    public String information; //email address/phone number

    @ColumnInfo
    public String autosender;

    @ColumnInfo
    public String message;

    public FriendEntity(){}

    public FriendEntity(int uid ,String fullname, String birthday ,String kindOfInfo ,String information , String autoSender, String message){
        this.uid = uid;
        this.fullName = fullname;
        this.birthday = birthday;
        this.kindOfInfo = kindOfInfo;
        this.information = information;
        this.autosender = autoSender;
        this.message = message;
    }

    public LocalDate convertToLocalDateViaInstant(Date dateToConvert) {
        return dateToConvert.toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDate();
    }

    public LocalDate convertStringToLocalDate(String dateToConvert) {
        Date date1 = new Date();
        try {
            date1=new SimpleDateFormat("dd/MM/yyyy").parse(dateToConvert);

        } catch (Exception e) {
            Log.e(MYTAG, e.getMessage());
        }
        return convertToLocalDateViaInstant(date1);
    }

    public int getUid() {
        return uid;
    }
    public String getFullName() {
        return fullName;
    }
    public String getBirthday() {
        return birthday;
    }
    public String getAge(){
        return valueOf(Period.between(convertStringToLocalDate(this.birthday), java.time.LocalDate.now()).getYears());
    }
    public String getKindOfInfo() { return kindOfInfo; }
    public String getInformation() {
        return information;
    }
    public String getAutosender() {
        return autosender;
    }
    public String getMessage() {
        return message;
    }
    public int getDaysForBirthday(){
        LocalDate today = LocalDate.now();
        LocalDate birthday = convertStringToLocalDate(this.birthday);
        LocalDate nextBDay = birthday.withYear(today.getYear());
        //If your birthday has occurred this year already, add 1 to the year.
        if (nextBDay.isBefore(today) || nextBDay.isEqual(today)) {
            nextBDay = nextBDay.plusYears(1);
        }
        if(today.getDayOfMonth()-birthday.getDayOfMonth()+today.getMonthValue()-birthday.getMonthValue()==0) return 0;
        return (int)ChronoUnit.DAYS.between(today, nextBDay);
    }

    //-----------------------------------------------------------------------

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }
    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }
    public void setKindOfInfo(String kindOfInfo) { this.kindOfInfo = kindOfInfo;}
    public void setInformation(String information) {
        this.information = information;
    }
    public void setAutosender(String autosender) {
        this.autosender = autosender;
    }
    public void setMessage(String message) {
        this.message = message;
    }

    @NonNull
    @Override
    public String toString() {
        return "\nname: "+fullName+"\nbirthday: "+birthday+"\nkindOfInfo"+kindOfInfo+"\ninformation"+information+"\nautosender: "+autosender+""+"\nmessage: "+message;

    }
}
