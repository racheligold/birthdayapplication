package com.example.loeiapp;

import android.Manifest;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings;
import android.telephony.SmsManager;
import android.widget.Toast;
import com.creativityapps.gmailbackgroundlibrary.BackgroundMail;
import java.util.List;
import androidx.core.content.ContextCompat;


public class MyBroadcastReceiver extends BroadcastReceiver {

    private static Activity mActivity;
    AppDatabase instance;

    public MyBroadcastReceiver() {
    }

    public static void setActivity (Activity mactivity){
        mActivity = mactivity;
    }

    public static boolean isAirplaneModeOn(Context context) {
        return Settings.Global.getInt(context.getContentResolver(),
                Settings.Global.AIRPLANE_MODE_ON, 0) != 0;
    }


    public static boolean haveNetworkConnection() {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) mActivity.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        return haveConnectedWifi || haveConnectedMobile;
    }

    @Override
    public void onReceive(final Context context, Intent intent) {
        instance = AppDatabase.getAppDatabase(context.getApplicationContext());
        final FriendDAO myDAO = instance.friendDao();

        List<MessageListEntity> messageListDB = myDAO.getAllMessages(); //get all messages that weren't send
        if(messageListDB.size()>0){
            for(final MessageListEntity message : messageListDB){ //also we already know that everyone that is on this message list want his message to auto send
                FriendEntity friendEntity = myDAO.loadFriendById(message.getuId());
                String type = friendEntity.getKindOfInfo(); //sms or mail
                switch (type){
                    case "sms":
                        if (!isAirplaneModeOn(context)) {
                            //sending
                            int readPhonePermissionCheck = ContextCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE);
                            int sendSMSPermissionCheck = ContextCompat.checkSelfPermission(context, Manifest.permission.SEND_SMS);

                            if (readPhonePermissionCheck == PackageManager.PERMISSION_GRANTED && sendSMSPermissionCheck == PackageManager.PERMISSION_GRANTED) { //we got permission to send sms and get phone state
                                if(!friendEntity.getMessage().isEmpty() && !friendEntity.getInformation().isEmpty()){
                                    SmsManager smsManager = SmsManager.getDefault();
                                    smsManager.sendTextMessage(friendEntity.getInformation(), null, friendEntity.getMessage(), null, null);
                                    Toast.makeText(context, "SMS sent.",Toast.LENGTH_LONG).show();
                                    myDAO.setMessageSent(message.getmId());
                                }
                            }
                        }
                        break;

                    case "mail":
                        if (haveNetworkConnection()) {
                            //sending email
                           if(!friendEntity.getMessage().isEmpty() && !friendEntity.getInformation().isEmpty()){
                                BackgroundMail.newBuilder(mActivity)
                                        .withUsername("happybirthdaypro3@gmail.com")
                                        .withPassword("HAPPYBIRTHDAY15521")
                                        .withMailto(friendEntity.getInformation())
                                        .withType(BackgroundMail.TYPE_PLAIN)
                                        .withSubject(friendEntity.getFullName()+" Happy Birthday!")
                                        .withBody(friendEntity.getMessage())
                                        .withOnSuccessCallback(new BackgroundMail.OnSuccessCallback() {
                                            @Override
                                            public void onSuccess() {
                                                myDAO.setMessageSent(message.getmId());
                                            }
                                        })
                                        .withOnFailCallback(new BackgroundMail.OnFailCallback() {
                                            @Override
                                            public void onFail() {
                                                //Toast.makeText(context, "Failed to send email.",Toast.LENGTH_LONG).show();
                                            }
                                        })
                                        .send();
                            }
                        }
                        break;
                    default: break;
                }
            }
        }
    }
}