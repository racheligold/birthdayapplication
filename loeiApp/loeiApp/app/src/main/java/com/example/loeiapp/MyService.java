package com.example.loeiapp;

import android.Manifest;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.os.IBinder;
import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.List;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.content.ContextCompat;


public class MyService extends Service {
    private Handler h;
    private Runnable r;
    String GROUP_KEY_MESSAGES = "MESSAGES";
    LocalDate sentDate = java.time.LocalDate.now();
    public static boolean isServiceRunning = false;
    boolean FirstNorificationDisplayed = false;
    boolean first = true;
    int summaryIndex;
    AppDatabase instance;
    ArrayList<Notification> notificationsList = new ArrayList<Notification>();

    @Override
    public void onCreate() {
        super.onCreate();
        instance = AppDatabase.getAppDatabase(getApplicationContext().getApplicationContext());
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null && intent.getAction().equals("start")) {
            if (isServiceRunning) return START_STICKY; //to avoid overlapping servers
            isServiceRunning = true;
            final Context c = this;

            h = new Handler();
            r = new Runnable() {
                @Override
                public void run() {
                    FriendDAO myDAO = instance.friendDao();
                    List<FriendEntity> friendsEntityDB = myDAO.getAll();
                    int daysForBirthday;
                    boolean somethingToSend = false;
                    boolean notificationToSend = false;

                    //at the end of the day we remove from the messageList all the messages that were sent
                    if(Period.between(sentDate, java.time.LocalDate.now()).getDays()>0){ //end of the day
                        if(myDAO.countAllSentMessages()>0) {
                            myDAO.deleteAllSentMessages();
                            notificationsList.clear();
                            FirstNorificationDisplayed = false;
                        }
                        sentDate = java.time.LocalDate.now(); //will keep the last sent message date
                    }

                    for (FriendEntity friendEntity : friendsEntityDB) {
                        daysForBirthday = friendEntity.getDaysForBirthday();
                        if(daysForBirthday == 0){ //today is his birthday
                            if(friendEntity.getAutosender().equals("Yes")){ //the user want to auto send him the message
                                if(myDAO.isMessageExistFor(friendEntity.getUid())==0){ //we didn't already send this message today
                                    if(friendEntity.getKindOfInfo().equals("sms")){ //we send by sms
                                        if(!MyBroadcastReceiver.isAirplaneModeOn(c)){ //making sure we are not on airplane mode
                                            myDAO.insert( new MessageListEntity(myDAO.rowMessageCount()+1 ,friendEntity.getUid(),"No"));
                                            int readPhonePermissionCheck = ContextCompat.checkSelfPermission(c, Manifest.permission.READ_PHONE_STATE);
                                            int sendSMSPermissionCheck = ContextCompat.checkSelfPermission(c, Manifest.permission.SEND_SMS);
                                            if (readPhonePermissionCheck != PackageManager.PERMISSION_GRANTED || sendSMSPermissionCheck != PackageManager.PERMISSION_GRANTED) { //we didnt get permission to send sms or get phone state
                                                notificationsList.add(updateNotification("The birthday message to "+friendEntity.getFullName()+" wasn't sent due to no permission! give the app permission and restart the service."));
                                            } else {
                                                if(!friendEntity.getMessage().isEmpty() && !friendEntity.getInformation().isEmpty()){
                                                    somethingToSend = true;
                                                    notificationsList.add(updateNotification(friendEntity.getFullName()+" Has a birthday today! his message was sent!"));
                                                }
                                                else {
                                                    notificationsList.add(updateNotification("The birthday message to "+friendEntity.getFullName()+" wasn't sent due to empty fields! fill the fields and restart the service."));
                                                }
                                            }
                                        }
                                        else{
                                            notificationsList.add(updateNotification("The birthday message to "+friendEntity.getFullName()+" wasn't sent due to Airplane Mode"));
                                        }
                                    }
                                    else{ //email
                                        myDAO.insert( new MessageListEntity(myDAO.rowMessageCount()+1 ,friendEntity.getUid(),"No"));
                                        if (haveNetworkConnection()) { //making sure we have internet connection
                                            if(!friendEntity.getMessage().isEmpty() && !friendEntity.getInformation().isEmpty()){
                                                somethingToSend = true;
                                                notificationsList.add(updateNotification(friendEntity.getFullName()+" Has a birthday today! his message was sent!"));
                                            }else{
                                                notificationsList.add(updateNotification("The birthday message to "+friendEntity.getFullName()+" wasn't sent due to empty fields! fill the fields and restart the service."));
                                            }
                                        } else {
                                            notificationsList.add(updateNotification("The birthday message to "+friendEntity.getFullName()+" wasn't sent due to no internet connection"));
                                        }
                                    }
                                    notificationToSend = true;
                                }
                            }
                        }
                    }

                    if(somethingToSend) { //checking if there is something to send
                        Intent i = new Intent(c, MyBroadcastReceiver.class);
                        c.sendBroadcast(i); //activating out BroadcastReceiver to send all the messages
                    }
                    if(notificationToSend){
                        //create a summary notification
                        if(first){ //there are few notifications to display so we will display them inside a summary group
                            summaryIndex = notificationsList.size();
                            notificationsList.add(new NotificationCompat.Builder(getApplicationContext(), "Birthday Reminder")
                                    .setContentTitle(getResources().getString(R.string.app_name))
                                    //set content text to support devices running API level < 24
                                    .setContentText("new messages")
                                    .setSmallIcon(android.R.drawable.stat_notify_chat)
                                    //specify which group this notification belongs to
                                    .setGroup(GROUP_KEY_MESSAGES)
                                    //set this notification as the summary for the group
                                    .setGroupSummary(true)
                                    .build());
                            first = false;
                        }
                        //notify the user by flouting the notifications
                        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(getApplicationContext());
                        for(int j = 0; j < notificationsList.size(); j++){
                            notificationManager.notify(j, notificationsList.get(j));
                        }
                        startForeground(summaryIndex, notificationsList.get(summaryIndex));

                    }else if(!FirstNorificationDisplayed) {
                        FirstNorificationDisplayed = true;
                        startForeground(101, updateNotification("Service is on"));
                    }

                    //myDAO.deleteAllMessages();
                    h.postDelayed(this, 30000); //3600000 every hour
                }
            };
            h.post(r);
        } else {
            if (isServiceRunning){
                h.removeCallbacks(r);
                stopMyService();
                isServiceRunning = false;
                FirstNorificationDisplayed = false;
            }
        }
        return START_STICKY; //this way the Service will open itself when the app is closed
    }


    public boolean haveNetworkConnection() {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) getApplication().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        return haveConnectedWifi || haveConnectedMobile;
    }

    // In case the service is deleted or crashes some how
    @Override
    public void onDestroy() {
        isServiceRunning = false;
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        // Used only in case of bound services.
        return null;
    }

    void stopMyService() {
        isServiceRunning = false;
        stopForeground(true);
        stopSelf();
    }

    private Notification updateNotification(String info) {
        Context context = getApplicationContext();
        PendingIntent action = PendingIntent.getActivity(context,
                0, new Intent(context, MainActivity.class),
                PendingIntent.FLAG_CANCEL_CURRENT); // Flag indicating that if the described PendingIntent already exists, the current one should be canceled before generating a new one.
        NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationCompat.Builder builder;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            String CHANNEL_ID = "Birthday Reminder";
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, "Birthday Reminder",
                    NotificationManager.IMPORTANCE_HIGH);
            channel.setDescription("Birthday Reminder Description");
            manager.createNotificationChannel(channel);
            builder = new NotificationCompat.Builder(this, CHANNEL_ID);
        }
        else
        {
            builder = new NotificationCompat.Builder(context);
        }
        Bitmap icon = BitmapFactory.decodeResource(getResources(), android.R.drawable.stat_notify_chat);
        CharSequence cs = info;
        return builder.setContentIntent(action)
                .setContentTitle(getResources().getString(R.string.app_name))
                .setTicker(getResources().getString(R.string.app_name))
                .setContentText(cs) //show this text on popup and later in the channel
                .setGroup(GROUP_KEY_MESSAGES)
                .setSmallIcon(android.R.drawable.stat_notify_chat)
                .setLargeIcon(Bitmap.createScaledBitmap(icon, 128, 128, false))
                .setContentIntent(action)
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(cs)) //stretch the info in the channel so it will show all the content of the message
                .setOngoing(true).build();
    }
}