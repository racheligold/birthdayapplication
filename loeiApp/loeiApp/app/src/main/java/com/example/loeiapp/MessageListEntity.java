package com.example.loeiapp;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;


@Entity
public class MessageListEntity {

    @PrimaryKey
    public int mid;

    @ColumnInfo
    public int uid;

    @ColumnInfo
    public String wasSent;

    public MessageListEntity(){

    }


    public MessageListEntity(int mid ,int uid , String wasSent){
        this.mid = mid;
        this.uid = uid;
        this.wasSent = wasSent;
    }

    public int getmId() {
        return mid;
    }
    public int getuId() {
        return uid;
    }
    public String getWasSent() {
        return wasSent;
    }

    //-----------------------------------------------------------------------
    public void setWasSent(String wasSent) {
        this.wasSent = wasSent;
    }

    @NonNull
    @Override
    public String toString() {
        return "\nmid: "+mid+"\nuid: "+uid+"\nwasSent "+wasSent;

    }
}
