package com.example.loeiapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

public class GalleryActivity extends AppCompatActivity implements View.OnClickListener {
    ImageView addPicture, refresh;
    ListView lv;
    Helper helper;
    GalleryAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery);
        connectComponents();
        addPicture.setOnClickListener(this);
        refresh.setOnClickListener(this);
    }

    private void connectComponents() {
        addPicture = (ImageView)findViewById(R.id.addNewPicture);
        refresh = (ImageView)findViewById(R.id.refresh);
        lv = (ListView)findViewById(R.id.galleryLv);
        helper = new Helper(this);
        adapter = new GalleryAdapter(this, 0, 0, helper.getPictures());
        lv.setAdapter(adapter);
    }

    @Override
    public void onClick(View v) {
        if(v == addPicture) //if the user wants to add picture
        {
            Intent i = new Intent(this, AddPictureActivity.class);
            startActivity(i);
        }
        if(v == refresh) //check if there are new pictures
        {
            if(helper.getPictures().size() != adapter.getObjects().size())
            {
                adapter.add(helper.getPictures().get(0));
                adapter.notifyDataSetChanged();
                Toast.makeText(this, "Now, everything is up to date", Toast.LENGTH_SHORT).show();
            }
            else
            {
                Toast.makeText(this, "Everything is up to date", Toast.LENGTH_SHORT).show();
            }
        }
    }
}